/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sieteymedia;

import java.util.Arrays;
import java.util.Scanner;
import recursos.Baraja;
import static recursos.Baraja.nuevoMazo;
import recursos.Carta;

/**
 *
 * @author monkeyfinger
 */
public class GameController {

    private static Baraja baraja;
    private static Jugador jugador;
    private static int apuesta;
    

    public static void juegaMano(int apuesta) {
        System.out.println("\nTus cartas son:");
        Carta nuevaCarta = new Carta("oros",4);
        System.out.println(nuevaCarta.toString());
        //System.out.println( nuevaCarta.toString());
    }
    
    public static void creaJugador(){
        System.out.println("Cómo te llamas?");
        Scanner z = new Scanner(System.in);
        String nombreJugador = z.nextLine();
        jugador = new Jugador(nombreJugador);
        System.out.println("Bienvenido, " + Jugador.nombre + ". Vamos a jugar!");
    }
    
    public static void apostar(){
        Scanner z = new Scanner(System.in);
        System.out.println("¿Cuánto deseas apostar? (mín: 10 créditos)");
        apuesta = z.nextInt();
        if (apuesta < 10 || apuesta > Jugador.credito){
            System.out.println("Debe meter un importe superior a 10 créditos e inferior a su crédito actual");
            apuesta = z.nextInt();
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        creaJugador();
        GameController gc = new GameController();        
        gc.imprimeReglas();
        apostar();
        juegaMano(apuesta);
    }

    public void imprimeReglas() {
        System.out.println("Pero antes, las reglas:\n" + "- Yo haré de banca\n" + "- Antes de pedir una carta, debes hacer una apuesta.\n"
                + "- La apuesta no puede ser inferior a 10\n" + "- Puedes sacar todas las cartas que quieras. Recuerda, las figuras (10, 11 y 12) valen medio punto y, el resto, su valor\n"
                + "- Si la suma de los valores de las cartas sacadas es superior a 7 y medio, se pierde\n" + "- Puedes plantarte en cualquier momento\n"
                + "- Yo, al ser la banca, estoy obligado a sacar cartas hasta superar tu jugada o pasarme\n" + "- Ganas si obtienes una jugada de valor superior a la mía\n"
                + "- En caso de empate, gano yo\n" + "- En caso de que uno de los dos saque 7 y media, se pagará el doble\n" + "- En caso de quedarte sin crédito, el juego finalizará\n\nTu crédito actual es de: " + Jugador.credito + " créditos\n" + "\nEmpecemos!!!\n");
    }
}
