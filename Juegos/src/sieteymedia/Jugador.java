/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sieteymedia;

import recursos.Baraja;
import recursos.Carta;

/**
 *
 * @author monkeyfinger
 */
public class Jugador {
   public static String nombre;
   public static Carta[] cartas;
   public static int credito;
   
   public Jugador (String nombre, int credito){
       this.nombre = nombre;
       Jugador.credito = credito;
   }
   
   public Jugador (String nombre){
       this.nombre = nombre;
       Jugador.credito = 1000;
   }
    
   public int actualuzaCredito(int resultadoJugada){
       credito = credito - resultadoJugada;
       return credito;
   }
   public String getNombre(){
       return nombre;
   }
   
   public Carta[] getCartas(){
       return cartas;
   }
   
   public static Carta[] nuevaCarta(){
       int otra = 1;
       return Baraja.daCartas(otra);
//       return Baraja.cartasMano;
   }
   
   public void reiniciaMano(){
       Baraja.cartasMano = null;
   }
   
   
}
