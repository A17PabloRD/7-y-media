/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursos;

import java.util.Random;
import sieteymedia.Jugador;

/**
 *
 * @author monkeyfinger
 */
public class Baraja {
    public static Carta[] cartas = new Carta[40];
    
    public static Carta nuevoMazo[] = new Carta[40];
    public static Carta cartasDadas[] = new Carta[40];
    public static Carta cartasMano[] = new Carta[40];
    
    public Baraja(){
        int[] valores = {1, 2, 3, 4, 5, 6, 7, 10, 11, 12};
        String[] palos = {"Oros", "Bastos", "Copas", "Espadas"};
        int contador = 0;
        for (int i = 0; i < palos.length; i++){
            for (int e = 0; e < valores.length; e++){
                nuevoMazo[contador] = new Carta(palos[i], valores[e]);
            }
        }
    }
    
    public static void barajaCartas(){
        Random ran = new Random();
        for (int i = 0; i < cartas.length; i++){
            int mezcla = ran.nextInt(cartas.length);
            Carta auxiliar = cartas[i];
            cartas[i] = cartas[mezcla];
            cartas[mezcla] = auxiliar;
        }
    }
    
    public static Carta[] cartasDadas(){
        int i = cartasDadas.length;
        for (int e = 0; i < cartasDadas.length + cartasMano.length; i++){
            cartasDadas[i] = cartasMano[e];
        }
        return cartasDadas;
    }
    
    public static Carta[] daCartas(int pNumCartas){
        for (int i = 0; i < pNumCartas; i++){
            cartasMano[i] = cartas[i];            
        }
        borraCartas(pNumCartas, nuevoMazo);
        return cartasMano;
    }        
    
    public static void borraCartas(int pNumCartas, Carta nuevoMazo[]){
        int i = pNumCartas;
        for (; i < nuevoMazo.length - 1; i++) {
            nuevoMazo[i] = nuevoMazo[i + 1];
        }
        nuevoMazo[i] = null;
    }
    
    public Carta[] cartasMazo(){
        return nuevoMazo;
    }
    
    public void reiniciaBaraja(){
        nuevoMazo = null;
        cartasMano = null;
        cartasDadas = null;
    }
}
