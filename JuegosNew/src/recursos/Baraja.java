/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursos;

import java.util.Random;

/**
 *
 * @author monkeyfinger
 */
public class Baraja {

    public static Carta mazoBarajado[] = new Carta[40]; //Baraja barajada
    public static Carta quedanEnMazo[] = new Carta[40]; //Las que quedan
    public static Carta cartasDadas[] = new Carta[40];  //Las que salieron
    public static Carta cartasMano[] = new Carta[10];   //Las que están en juego
    public static Carta[] cartas = new Carta[40];
    public static int contadorCreaBaraja = 0;

    // Constructor de baraja
    public Baraja() {
        int[] valores = {1, 2, 3, 4, 5, 6, 7, 10, 11, 12};
        String[] palos = {"OROS", "BASTOS", "COPAS", "ESPADAS"};
        int contador = 0;
        for (int i = 0; i < palos.length; i++) {
            for (int e = 0; e < valores.length; e++) {
                mazoBarajado[contador] = new Carta(palos[i], valores[e]);
                contadorCreaBaraja++;
            }
        }
    }

    // Baraja la baraja
    public static void barajaCartas() {
        Random ran = new Random();
        for (int i = 0; i < mazoBarajado.length; i++) {
            int mezcla = ran.nextInt(mazoBarajado.length);
            Carta auxiliar = mazoBarajado[i];
            mazoBarajado[i] = mazoBarajado[mezcla];
            mazoBarajado[mezcla] = auxiliar;
        }
//        for (int i = 0; i < mazoBarajado.length; i++){
//            quedanEnMazo[i] = mazoBarajado[i];
//        }
    }

    // Devuelve la relacion de cartas que ya salieron
    public static Carta[] cartasDadas() {
        return cartasDadas;
    }

    // Devuelve relación cartas que siguen en el mazo
    public static Carta[] cartasMazo() {
        return quedanEnMazo;
    }

    public static Carta[] daCartas(int pNumCartas) {
        for (int i = 0; i < pNumCartas; i++) {
            cartasMano[i] = mazoBarajado[i];
            borraCartas(1, quedanEnMazo);
        }
        return cartasMano;
    }

    public static void borraCartas(int pNumCartas, Carta quedanEnMazo[]) {
        System.arraycopy(mazoBarajado, pNumCartas + 1, quedanEnMazo, pNumCartas, quedanEnMazo.length - pNumCartas);
    }

    public static void reiniciaBaraja() {
        mazoBarajado = null;
        cartasMano = null;
        cartasDadas = null;
        quedanEnMazo = null;
    }
}
