/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursos;

/**
 *
 * @author monkeyfinger
 */
public class Carta {

    String palo;
    int valor;

    public Carta(String pPalo, int pValor) {
        this.palo = pPalo;
        this.valor = pValor;
    }

    @Override
    public String toString() {
        if (valor < 7) {
            switch (valor) {
                case 10:
                    return "[" + palo + " - " + "SOTA]";
                case 11:
                    return "[" + palo + " - " + "CABALLO]";
                default:
                    return "[" + palo + " - " + "REY]";
            }
        }
        return "[" + palo + " - " + valor + "]";
    }

    public int getValor() {
        return this.valor;
    }
}
