/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sieteymedia;

import recursos.Baraja;
import recursos.Carta;
import static sieteymedia.Jugador.cartas;

/**
 *
 * @author monkeyfinger
 */
public class Jugador {

    public static String nombre;
    public static Carta[] cartas;
    public static int credito;

    public Jugador(String nombre, int credito) {
        this.nombre = nombre;
        Jugador.credito = credito;
    }

    public Jugador(String nombre) {
        this.nombre = nombre;
        Jugador.credito = 1000;
//       Jugador.cartas = {};
    }

    public int actualuzaCredito(int resultadoJugada) {
        credito = credito - resultadoJugada;
        return credito;
    }

    public String getNombre() {
        return nombre;
    }

    public Carta[] getCartas() {
        return cartas;
    }

    public static void setCartas(Carta cartaNueva, int i) {
        Jugador.cartas[i] = cartaNueva;
    }

    public static void nuevaCarta(Carta carta) {

    }

    public void reiniciaMano() {
        Baraja.cartasMano = null;
    }

}
